import React, { useState } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import Game from './Game';
import Leaderboard from './Leaderboard';
import TopBar from './TopBar';
import UserProfile from './UserProfile';

const GameScreen = ({ username, onLogout }) => {
  const [key, setKey] = useState(0);

  const handleGameEnd = () => {
    setKey((prevKey) => prevKey + 1);
  };

  return (
    <div>
      <TopBar username={username} onLogout={onLogout} />
      <div className="main-content">
        <Routes>
          <Route
            path="/game"
            element={
              <>
                <UserProfile key={key} username={username} />
                <Game username={username} onGameEnd={handleGameEnd} />
              </>
            }
          />
          <Route path="/leaderboard" element={<Leaderboard />} />
          <Route path="*" element={<Navigate to="/game" />} />
        </Routes>
      </div>
    </div>
  );
};

export default GameScreen;
