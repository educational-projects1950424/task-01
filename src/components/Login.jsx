import React, { useState } from 'react';

function Login({ onLogin }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isRegistering, setIsRegistering] = useState(false);

  const handleLogin = () => {
    const storedUser = localStorage.getItem(username);
    if (storedUser) {
      const user = JSON.parse(storedUser);
      if (user.password === password) {
        onLogin(username);
      } else {
        alert('Invalid password');
      }
    } else {
      alert('User not found');
    }
  };

  const handleRegister = () => {
    if (localStorage.getItem(username)) {
      alert('User already exists');
    } else {
      localStorage.setItem(username, JSON.stringify({ username, password }));
      onLogin(username);
    }
  };

  return (
    <div className="login">
      <h2>{isRegistering ? 'Register' : 'Tic-Tac-Toe'}</h2>
      <div className="label-username">
        <label htmlFor="username-input">Nickname:</label>
        <input
          className="input-text"
          type="text"
          id="username-input"
          placeholder="Enter your username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
      </div>
      <div className="label-password">
        <label htmlFor="password-input">Password:</label>
        <input
          className="input-text"
          type="password"
          id="password-input"
          placeholder="Enter your password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>

      {isRegistering ? (
        <button onClick={handleRegister}>Register</button>
      ) : (
        <button onClick={handleLogin}>Login</button>
      )}
      <button onClick={() => setIsRegistering(!isRegistering)}>
        {isRegistering
          ? 'Already have an account? Login'
          : 'Need an account? Register'}
      </button>
    </div>
  );
}

export default Login;
