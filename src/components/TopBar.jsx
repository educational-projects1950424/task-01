import React from 'react';
import { Link } from 'react-router-dom';

const TopBar = ({ username, onLogout }) => {
  return (
    <div className="container-topbar">
      <div className="topbar">
        <div className="username">Hello, {username}</div>
        <div className="menu">
          <ul className="menu-ul">
            <li>
              <Link to="/game">Game</Link>
            </li>
            <li>
              <Link to="/leaderboard" className="leaderboard-li">
                Leaderboard
              </Link>
            </li>
            <li>
              <button className="logout" onClick={onLogout}>
                Log Out
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default TopBar;
