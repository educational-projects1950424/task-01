import React, { useEffect, useState } from 'react';

const mockLeaderboardData = [
  { username: 'player1', wins: 10, losses: 5, games: 15 },
  { username: 'player2', wins: 8, losses: 7, games: 15 },
];

function Leaderboard() {
  const [leaderboard, setLeaderboard] = useState([]);

  const loadLeaderboard = () => {
    const savedLeaderboard =
      JSON.parse(localStorage.getItem('leaderboard')) || mockLeaderboardData;

    const sortedLeaderboard = savedLeaderboard
      .sort((a, b) => b.wins - a.wins)
      .slice(0, 10);

    setLeaderboard(sortedLeaderboard);
  };

  useEffect(() => {
    loadLeaderboard();
  }, []);

  return (
    <div className="leaderboard">
      <table>
        <thead>
          <tr>
            <th>Username</th>
            <th>Wins</th>
            <th>Losses</th>
            <th>Games</th>
            <th>Score</th>
          </tr>
        </thead>
        <tbody>
          {leaderboard.map((player, index) => (
            <tr key={index}>
              <td>{player.username}</td>
              <td>{player.wins}</td>
              <td>{player.losses}</td>
              <td>{player.games}</td>
              <td>{player.wins - player.losses}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Leaderboard;
