import React, { useEffect, useState } from 'react';

const UserProfile = ({ username }) => {
  const [userData, setUserData] = useState({
    wins: 0,
    losses: 0,
    games: 0,
    score: 0,
  });

  const loadUserData = () => {
    const leaderboard = JSON.parse(localStorage.getItem('leaderboard')) || [];
    const userStats = leaderboard.find(
      (player) => player.username === username
    );

    if (userStats) {
      setUserData({
        wins: userStats.wins,
        losses: userStats.losses,
        games: userStats.games,
        score: userStats.wins - userStats.losses,
      });
    }
  };

  useEffect(() => {
    loadUserData();
  }, [username]);

  return (
    <div className="user-profile">
      <p>Wins: {userData.wins}</p>
      <p>Losses: {userData.losses}</p>
      <p>Total games: {userData.games}</p>
      <p>Scores: {userData.score}</p>
    </div>
  );
};

export default UserProfile;
