import React from 'react';

function Profile({ user }) {
  return (
    <div className="profile">
      <h2>Profile</h2>
      <div>
        <strong>Username:</strong> {user}
      </div>
      <div>
        <strong>Total Wins:</strong> 0
      </div>
      <div>
        <strong>Total Losses:</strong> 0
      </div>
      <div>
        <strong>Total Games:</strong> 0
      </div>
    </div>
  );
}

export default Profile;
