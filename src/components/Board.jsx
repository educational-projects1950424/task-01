import React, { useEffect } from 'react';
import Square from './Square';
import calculateWinner from '../utils/calculateWinner';

function Board({ xIsNext, squares, onPlay, username }) {
  useEffect(() => {
    if (!xIsNext) {
      const emptySquares = squares
        .map((square, idx) => (square === null ? idx : null))
        .filter((val) => val !== null);
      if (emptySquares.length > 0) {
        const randomMove =
          emptySquares[Math.floor(Math.random() * emptySquares.length)];
        setTimeout(() => handleClick(randomMove), 500);
      }
    }
  }, [xIsNext, squares]);

  function handleClick(i) {
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    const nextSquares = squares.slice();
    nextSquares[i] = xIsNext ? 'X' : 'O';
    onPlay(nextSquares);
  }

  const result = calculateWinner(squares);
  const winner = result ? result.winner : null;
  const winningLine = result ? result.line : [];

  let status;
  if (winner) {
    status = 'Winner: ' + (winner === 'X' ? username : 'Bot');
  } else if (!squares.includes(null)) {
    status = 'Draw';
  } else {
    status = 'Next player: ' + (xIsNext ? username : 'Bot');
  }

  function renderSquare(i) {
    return (
      <Square
        value={squares[i]}
        onSquareClick={() => handleClick(i)}
        isWinningSquare={winningLine.includes(i)}
      />
    );
  }

  return (
    <>
      <div className="status">{status}</div>
      <div className="board-row">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="board-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="board-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </>
  );
}

export default Board;
