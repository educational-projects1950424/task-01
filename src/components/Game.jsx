import React, { useState, useEffect } from 'react';
import Board from './Board';
import calculateWinner from '../utils/calculateWinner';

export default function Game({ username, onGameEnd }) {
  const [history, setHistory] = useState([Array(9).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0);
  const [score, setScore] = useState(0);
  const [gameOver, setGameOver] = useState(false);
  const xIsNext = currentMove % 2 === 0;
  const currentSquares = history[currentMove];

  useEffect(() => {
    const result = calculateWinner(currentSquares);
    if (result && !gameOver) {
      if (result.winner === 'X') {
        setScore(score + 1);
      }
      setGameOver(true);
      saveResult(username, result.winner === 'X');
      onGameEnd(); // This will trigger a re-render of UserProfile and Leaderboard
    }
  }, [currentSquares, gameOver, score, username, onGameEnd]);

  function saveResult(username, isWinner) {
    const leaderboard = JSON.parse(localStorage.getItem('leaderboard')) || [];
    const existingPlayer = leaderboard.find(
      (player) => player.username === username
    );

    if (existingPlayer) {
      existingPlayer.wins += isWinner ? 1 : 0;
      existingPlayer.losses += isWinner ? 0 : 1;
      existingPlayer.games += 1;
    } else {
      leaderboard.push({
        username: username,
        wins: isWinner ? 1 : 0,
        losses: isWinner ? 0 : 1,
        games: 1,
      });
    }

    localStorage.setItem('leaderboard', JSON.stringify(leaderboard));
  }

  function handlePlay(nextSquares) {
    const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
    setHistory(nextHistory);
    setCurrentMove(nextHistory.length - 1);
    setGameOver(false);
  }

  function jumpTo(nextMove) {
    setCurrentMove(nextMove);
    setGameOver(false);
  }

  const moves = history.map((squares, move) => {
    let description;
    if (move > 0) {
      description = 'Go to move #' + move;
    } else {
      description = 'Go to game start';
    }
    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>{description}</button>
      </li>
    );
  });

  return (
    <div className="game">
      <div className="game-board">
        <Board
          xIsNext={xIsNext}
          squares={currentSquares}
          onPlay={handlePlay}
          username={username}
        />
      </div>
      <div className="game-info">
        <ol>{moves}</ol>
      </div>
    </div>
  );
}
