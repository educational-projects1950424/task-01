// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getAnalytics } from 'firebase/analytics';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyB1YkG1h0ku8z2ghoVD7BhcCaZKDmJ6Kmw',
  authDomain: 'tic-tac-toe-task-01.firebaseapp.com',
  projectId: 'tic-tac-toe-task-01',
  storageBucket: 'tic-tac-toe-task-01.appspot.com',
  messagingSenderId: '991572894965',
  appId: '1:991572894965:web:e0319ec05ba7087bbf91bc',
  measurementId: 'G-6BVHXFWXSE',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
