import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from 'react-router-dom';
import Login from './components/Login';
import GameScreen from './components/GameScreen';
import './styles/styles.css';

function App() {
  const [username, setUsername] = useState(
    localStorage.getItem('username') || ''
  );

  useEffect(() => {
    const storedUsername = localStorage.getItem('username');
    if (storedUsername) {
      setUsername(storedUsername);
    }
  }, []);

  const handleLogin = (username) => {
    setUsername(username);
    localStorage.setItem('username', username);
  };

  const handleLogout = () => {
    setUsername('');
    localStorage.removeItem('username');
  };

  return (
    <Router>
      <div className="app">
        {username ? (
          <Routes>
            <Route
              path="/*"
              element={
                <GameScreen username={username} onLogout={handleLogout} />
              }
            />
            <Route path="*" element={<Navigate to="/game" />} />
          </Routes>
        ) : (
          <Routes>
            <Route path="*" element={<Login onLogin={handleLogin} />} />
          </Routes>
        )}
      </div>
    </Router>
  );
}

export default App;
